#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>

#include "sctframe.h"
#include "scta.h"

FILE *sctframe_config_debug = NULL;
int sctframe_config_version[3] = {1, 0, 0};

sctframe_t *sctframe_new() {
    sctframe_t *f = malloc(sizeof(sctframe_t));
    if (!f) {
        // bad memory
        return NULL;
    }
    f->records = scta_new(struct record);
    if (!f->records) {
        // darray fail
        free(f);
        return NULL;
    } 
    return f;
}



void *sctframe_add_(sctframe_t *f, ...) 
{
    struct record r;
    
    va_list ap;
    va_start(ap, f);
    
    if (!(r.address = va_arg(ap, void *))) goto EXIT;
    r.freef = va_arg(ap, void (*)(void *));
    
    
    if (!scta_push(f->records, r)) {
        // could not store array
        r.address = NULL;
        goto EXIT;
    }
    
    EXIT:
    va_end(ap);
    return r.address;
}

void sctframe_free(sctframe_t *f) {
    for (int i = 0; i < scta_length(f->records); ++i) {
        if (f->records[i].freef) f->records[i].freef(f->records[i].address);
        else free(f->records[i].address);
    }
    scta_free(f->records);
    free(f);
}

