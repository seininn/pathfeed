
struct ll_t{
    struct ll_t *next;
    char *str;
};

struct ll_t* listAdd(struct ll_t* list, char *str){/*
    How to use:
        In general, you need TWO pointers when using this function
        on to use as a cursor and the other is the first node in the list, 
        which should be used to finally free the list and NOTHING else.
        
        orgin = marker = listAdd(NULL, "first string");
        
        Then simply pass the pointer you wish to insert after. Note that 
        inserting mid list is not currently supported (because I haven't 
        encountered a situation where I needed it.)
        
        marker = listAdd(marker, "second string"); // marker points to [0] then [1]
        marker = listAdd(marker, "third string"); // marker points to [1] then [2]
        
*/

    // Handle case whith mid-insertion, no new null node needed
    // A terminating node
    struct ll_t *node;
    
    if (!(node = malloc(sizeof(struct ll_t)))) {
        exit(12);
    }
    
    // Copy the string
    char *s;
    if (!(s = malloc(strlen(str)+1))) {
        exit(12);
    }
    strcpy(s, str);
    
    
    // If a new list
    if (list) {
        list->next = node;
        list = list->next;
    } 
    else {
        list = node;
    }
    
    list->str  = s;
    list->next = NULL;
    return list;
}

void listFree(struct ll_t* list){
    struct ll_t* prev;
    while (list){
        prev = list;
        free(list->str);
        list = list->next;
        free(prev);
    }
}
/*
void listTest(){
    //test for leaks
    struct ll_t *mylist = NULL, *original;
    
    original = mylist = listAdd(mylist, "hi");
    mylist = listAdd(mylist, "my");
    mylist = listAdd(mylist, "name");
    mylist = listAdd(mylist, "is");
    mylist = listAdd(mylist, "tom");
 
    mylist = original;
    while (mylist){
        fprintf(stderr, "%s ", mylist->str);
        mylist = mylist->next;
    }
    puts("");
    listFree(original);
}*/



