/*
    sctstr: A simple string module 

    Sulaiman (seininn) Mustafa     2014-01-19
    Version 0.1
    
    This module is a part of libstc.
*/
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

FILE *sctstr_config_debug = NULL;

/*
bool strDup(char **o, char *s){
    char *r;
    if (!(r = malloc(strlen(s)+1))) {
        *o = NULL;
        return false;
    }
    strcpy(r, s);
    *o = r;
    return true;
}*/

bool sctstr_append(char **o, char *s){
    char *temp;
    int len = strlen((*o)?(*o):"");
    if (!(temp = realloc(*o, len+strlen(s)+1))) {
        if (sctstr_config_debug) fprintf(sctstr_config_debug, "Out of memory");
        return false;
    }
    else {
        *o = temp;
    }
    if (len == 0) **o = 0;
    strcat(*o, s);
    return true;
}
