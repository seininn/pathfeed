/*

    scthtml: Usefull HTML Routines
    Sulaiman (seininn) Mustafa
    Version 1:0:1
    Date: 2014-01-21
    Dependency list: sctstr
    
    This is a module of the sctlibz collection; sctlibs is a 
    nerve-wrekingly inefficiant library zoo.
    
    


    ---
    TODO
        add sanity tests later
            
    Nuggets:
        define global variables for general configuration, consider using structs?
*/

#define _POSIX_C_SOURCE 200809L

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <unistd.h>
#include <errno.h>
#include "sctstr.h"



FILE *scthtml_config_debug = NULL;
int scthtml_config_version[3] = {1, 0, 1};

char *scthtml_percent_encode(char *s, char *exceptions){
    // implemented what I needed, prob. doesn't conferm to spec. Fix later
    char *out = NULL;
    
    if (!exceptions) exceptions = "";
    
    for (;*s;++s) {
        if ((*s >= 0x20 && *s < 0x30) && !strchr(exceptions, *s)) {
            char buf[4];
            snprintf(buf, 4, "%%%x", *s);
            sctstr_append(&out, buf);
        }
        else sctstr_append(&out, (char [2]) {*s, 0});
    }
    return out;
}
char *scthtml_xml_escape(char *s, char *exceptions){
    // implemented what I needed, prob. doesn't conferm to spec. Fix later
    char *out = NULL, *ptr;
    char *list_in = "\"'<>&";
    char *list_out[] = {"quot", "apos", "lt", "gt", "amp"};
    
    if (!exceptions) exceptions = "";
    
    for (;*s;++s) {
        if ((ptr = strchr(list_in, *s)) && !strchr(exceptions, *s)) {
            char buf[7] = {0};
            strcat(buf, "&");
            strcat(buf, list_out[ptr-list_in]);
            strcat(buf, ";");
            sctstr_append(&out, buf);
        }
        else sctstr_append(&out, (char [2]) {*s, 0});
    }
    return out;    
}

char *scthtml_get_html_title(char *filename){
    // posible todo: currently reads full file in, then searches. Do it 
    // as a stream
    
    // Also consider using lim for a more robust method (will be equivilant to js getTextContent)
    int fd = open(filename, 0), rc;
    char buf[2053];
    if (fd == -1) {
        if (scthtml_config_debug) {
            fprintf(scthtml_config_debug, "Unable to open '%s' to extract title: %s\n", filename, strerror(errno));
        }
        return NULL;
    }
    rc = read(fd, buf, 2052);

    if (rc == -1) {
        if (scthtml_config_debug) {
            fprintf(scthtml_config_debug, "Unable to read from '%s': %s\n", filename, strerror(errno));
            perror(":");
        }
        return NULL;
    }
    buf[rc] = 0;
    
    close(fd);

    
    char *end, *start;
    
    start = strstr(buf, "<title");
    if (!start) return NULL;
    
    if (!(start = strstr(start, ">"))) return NULL;
    start+=1;
    if (!(end = strstr(start, "<"))) return NULL;
    
    start = strndup(start, end-start);
    
    if (strchr(start, '\n') || !strlen(start)) {
        free(start);
        return NULL;
    }
    else return start;
}

