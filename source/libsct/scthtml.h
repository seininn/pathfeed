/*

    scthtml: Usefull HTML Routines
    Sulaiman (seininn) Mustafa
    Version 1:0:1
    Date: 2014-01-21
    Dependency list: sctstr
    
    This is a module of the sctlibz collection; sctlibs is a 
    nerve-wrekingly inefficiant library zoo.
*/

#ifndef SCTHTML_H
#define SCTHTML_H

#include <stdbool.h>
#include <stdio.h>

extern FILE *scthtml_config_debug;
extern int scthtml_config_version[3];

char *scthtml_get_html_title(char *filename);
char *scthtml_percent_encode(char *s, char *exceptions);
char *scthtml_xml_escape(char *s, char *exceptions);

#endif
