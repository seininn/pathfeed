/*

    sctos: Usefull Operating System Routines
    Sulaiman (seininn) Mustafa
    Version 1:0:1
    Date: 2014-01-21
    Dependency list: sctstr
    
    This is a module of the sctlibz collection; sctlibs is a 
    nerve-wrekingly inefficiant library zoo.
    
    
    ---
    
    TODO
        Messy! factorize and add tests. This pile of code is a gigantic bug 
        waiting to happen.

*/

#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <unistd.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <stdio.h>

#include "sctstr.h"

FILE *sctos_config_debug = NULL;
bool sctos_config_verbose = true;
int sctos_config_version[3] = {1, 0, 1};

bool sctos_walk(char *path, bool (*handler)(void *payload, char *path), void *payload){
    // add decrementing maxdepth later
    // add fail mode strict or (default) permisive
    DIR *dfd;
    struct dirent *entry;
    bool rv = true;
    
    if (sctos_config_debug) fprintf(sctos_config_debug, "== looking in '%s'\n", path);
    if (!(dfd = opendir(path))) {
        if (sctos_config_verbose || sctos_config_debug) {
            fprintf(sctos_config_debug?sctos_config_debug:stderr, "could not open '%s': %s\n", path, strerror(errno));
        }
        return false;
    }
    
    while ((entry = readdir(dfd))) {
        char *newpath = NULL;
        sctstr_append(&newpath, path);
        sctstr_append(&newpath, "/");
        sctstr_append(&newpath, entry->d_name);
        
        struct stat st;
        if (stat(newpath, &st) == -1) {
            if (sctos_config_verbose || sctos_config_debug) perror(newpath);
            rv = false;
            free(newpath);
            continue;
        }
        if (S_ISDIR(st.st_mode)) {
            if (!strcmp(entry->d_name, ".") || !strcmp(entry->d_name, "..")) {
                free(newpath);
                continue;
            }
            
            rv &= handler(payload, newpath);
            rv &= sctos_walk(newpath, handler, payload);
        }
        else {
            rv &= handler(payload, newpath);
        }
        free(newpath);
    }
    
    closedir(dfd);
    return rv;
}



