/*
    scta: Dynamic Arrays Module 

    Sulaiman (seininn) Mustafa     2014-02-17
    Version 0.1
    
    This module is a part of libstc, but can be used independently.
    
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <limits.h>

#include "scta.h"


FILE *scta_config_debug = NULL;
int scta_config_version[3] = {1, 0, 0};

/*
    internal array layout

    +------------------+
    |       SIZE       |  <- internal pointer points here
    +------------------+
    |      LENGTH      |
    +------------------+
    |      RETURN      |  used to be used for return status, might remove later
    +------------------+
    |   ARGUMENT_INT_1 |  argument regesters for functions to avoid multiple 
    +------------------+  macro argument invokations. unfortunetly, the array
    |   ARGUMENT_INT_2 |  argument must be invoked more than once. (that, or
    +------------------+  use statement expressions wich makes this non-c code)
    |   ARGUMENT_PTR_1 |  :
    +------------------+  :
    |   ARGUMENT_PTR_2 |  :
    +------------------+  '
    |     ELEMENT_0    |  <- external pointer points here 
    +------------------+
    +---------:--------+
    +---------:--------+
    +------------------+
    |     ELEMENT_N    |
    +------------------+
    |      STORAGE     |  (same size as normal array elements)
    +------------------+
    
*/

#define ERRMSG_AND_RETURN(RET, ...) {\
    if (scta_config_debug) {\
        fprintf(scta_config_debug, "scta:%s():%d: ", __func__, __LINE__);\
        fprintf(scta_config_debug, __VA_ARGS__);\
        fprintf(scta_config_debug, "\n");\
    }\
    return (RET); \
}



// XXX EXTERN SHOULD NOT APPEAR IN THIS DOCUMENT

bool make_space(void **ra, int at, int count){
    // makes a space inside *ra, and moves everything (including storage) to the right
    // count is the number of elements to remove and has nothing to do with size
    size_t size = SCTA__INTERN_SIZE(*ra)[0];
    int length = SCTA__INTERN_LEN(*ra)[0];
    void *temp;
    
    if (!size) 
        ERRMSG_AND_RETURN(false, "element size can not be zero.")
    
    if (length == INT_MAX) 
        ERRMSG_AND_RETURN(false, "cannot increase array size do to int limit.")
        
    if (!(temp=realloc(*ra, (SCTA_HEAD)+ size*length + size*count + size))) 
        ERRMSG_AND_RETURN(false, "memory operation failed: new size is %zu", ((SCTA_HEAD+size)+size*length+size))
    *ra = temp;
    
    
    memmove( ((char *) *ra) + SCTA_HEAD + at*size + size*count, 
             ((char *) *ra) + SCTA_HEAD + at*size, 
             (length*size) - (at*size) + size);
    
    
    SCTA__INTERN_LEN(*ra)[0]+=count;
    return true;
}

bool collapse_space(void **ra, int at, int count){
    // removes a space from *ra, and moves everything (including storage) to the left
    // count is the number of elements to remove and has nothing to do with size
    size_t size = SCTA__INTERN_SIZE(*ra)[0];
    int length = SCTA__INTERN_LEN(*ra)[0];
    void *temp;
    
    if (!size) 
        ERRMSG_AND_RETURN(false, "element size can not be zero.")
    
    if (!length) 
        ERRMSG_AND_RETURN(false, "cannot shorten a zero length array")
        
    if (at < length-1) {
        memmove( ((char *) *ra) + SCTA_HEAD + at*size, 
                 ((char *) *ra) + SCTA_HEAD + at*size + size*count, 
                 (length*size) - (at*size + size*count) + size );
    }
    
    if (!(temp=realloc(*ra, SCTA_HEAD + size*length - size*count + size))) 
        ERRMSG_AND_RETURN(false, "memory operation failed: new size is %zu", ((SCTA_HEAD+size)+size*length-size))
    *ra = temp;
    
    SCTA__INTERN_LEN(*ra)[0] -= count;
    return true;
}


void* make_raw_array(size_t size){
    void *ra;
    
    if (!size) 
        ERRMSG_AND_RETURN(NULL, "requested array member size is 0. minimum is 1.")
    
    if (!(ra = malloc(SCTA_HEAD + size)))
        ERRMSG_AND_RETURN(NULL, "could not allocate enough memory: requested %zu", SCTA_HEAD + size)
        
    memset(ra, 0, SCTA_HEAD + size);
    
    /* debugging macros
    fprintf(stderr, "base %p\n", c);
    fprintf(stderr, "size loc %p\n", SCTA__INTERN_SIZE(c));
    fprintf(stderr, "len loc %p\n", SCTA__INTERN_LEN(c));
    fprintf(stderr, "MTS: 0x%lx (0x%lx, 0x%lx, 0x%lx)\n", SCTA_HEAD, sizeof(size_t), sizeof(int), sizeof(bool));
    fprintf(stderr, "len loc %p\n", SCTA__INTERN_LEN(c));
    */
    
    SCTA__INTERN_SIZE(ra)[0] = size;
    SCTA__INTERN_LEN(ra)[0] = 0;
    
    return ra;
}

















// Macro functions. Dont forget:
// -- to differeantiat between internal functions and macro ones and:
// -- convert arrays to raw arrays and vice versa when nessary by sub/add SCTA__OFFSET_HEADER
// -- functions here must not use EXTERN functions for concistancy.
// -- arguments are stored in the array itself. the specification of this is in the function's comment
// -- arrays passed by macros are in there normal form and not raw form

void* scta_macro_new_(size_t size){/**/
    void *ra = make_raw_array(size);
    if (ra) return ((char *)ra)+SCTA_HEAD;
    else return NULL;
}

void scta_macro_free_(void *c){ free((char *)c-SCTA_HEAD); }


bool scta_macro_set_length_(char **z, int newl){
    void *ra = *z-SCTA_HEAD;
    int oldl = SCTA__INTERN_LEN(ra)[0];
    
    if (newl > oldl) {
        if (!make_space(&ra, oldl, newl-oldl)) return false;
        memset((char *)ra+SCTA_HEAD+(oldl*SCTA__INTERN_SIZE(ra)[0]), 0, (newl-oldl)*SCTA__INTERN_SIZE(ra)[0]);
    }
    if (newl < oldl) {
        if (!collapse_space(&ra, newl, oldl-newl)) return false;
    }
    
    *z = (char *) ra + SCTA_HEAD;
    return true;    
}

bool scta_macro_insert_(char **z){/*
    inserts an element in an array
    
    INT1 : at
    STORAGE: what
*/
    void *ra = *z-SCTA_HEAD;
    
    
    size_t size = SCTA__INTERN_SIZE(ra)[0];
    int oldl = SCTA__INTERN_LEN(ra)[0];

    int at = SCTA__INTERN_INT1(ra)[0];
    
    // if negative indexing: normalize
    if (at < 0) at = oldl + at;
    // if at is larger than length of array, set to insert to length
    if (at > oldl) at = oldl;
    
    
    if ( at < 0) 
        ERRMSG_AND_RETURN(false, "inconsistant paramiters:\n" 
                                 "\tlength: %d\n" 
                                 "\top index at: %d\n", oldl, at)
    
    if (!make_space(&ra, at, 1)) return false;
    
    memmove(    ((char *) ra) + SCTA_HEAD + size*at, 
                ((char *) ra) + SCTA_HEAD + size*(SCTA__INTERN_LEN(ra)[0]) , size);
    
    *z = (char *) ra + SCTA_HEAD;
    return true;
}

bool scta_macro_insert_array_(char **z){/*
    inserts an element in an array
    
    INT1 : at
    INT2 : length
    PTR1: pointer
*/
    void *ra = *z-SCTA_HEAD;    
    
    size_t size = SCTA__INTERN_SIZE(ra)[0];
    int oldl = SCTA__INTERN_LEN(ra)[0];

    int target_length = SCTA__INTERN_INT2(ra)[0];
    void *target_addr = SCTA__INTERN_PTR1(ra)[0];

    int at = SCTA__INTERN_INT1(ra)[0];
    
    // normalize index if negative
    if (at < 0) at = oldl + at;
    // set after last element if index is larger than array length
    if (at > oldl) at = oldl;
    
    if ( at < 0) 
        ERRMSG_AND_RETURN(false, "inconsistant paramiters:\n" 
                                  "\tlength: %d\n" 
                                 "\top index at: %d\n", oldl, at)
    
    if (!make_space(&ra, at, target_length)) return false;
    
    memmove(    ((char *) ra) + SCTA_HEAD + size*at, 
                target_addr , size*target_length);
    
    *z = (char *) ra + SCTA_HEAD;
    
    return true;
}


bool scta_macro_delete_subset_(char **z){/*
    deletes a subset that starts at `at' and has a length of `length'
    
    input
    
    INT1 : at
    INT2 : length
    
    output:
    STORAGE fist element in deleted subset
    
*/
    void *ra = *z-SCTA_HEAD;    
    
    size_t size = SCTA__INTERN_SIZE(ra)[0];
    int oldl = SCTA__INTERN_LEN(ra)[0];

    int target_length = SCTA__INTERN_INT2(ra)[0];

    int at = SCTA__INTERN_INT1(ra)[0];
    // normalze if index is negative
    if (at < 0) at = oldl + at;
    // set index BEFORE last element if larger than length (so that the last 
    // element is deleted
    if (at > oldl) at = oldl-1;
    
    if ( oldl < (at+target_length) || at < 0) 
        ERRMSG_AND_RETURN(false, "inconsistant paramiters:\n" 
                                 "\tlength: %d\n" 
                                 "\tdeletion starts at: %d\n" 
                                 "\tand spans %d element(s)\n", oldl, at, target_length)

    
    memmove( ((char *) ra) + SCTA_HEAD + size*oldl, 
             ((char *) ra) + SCTA_HEAD + size*at, size);
    
    if (!collapse_space(&ra, at, target_length)) return false;
    
    *z = (char *) ra + SCTA_HEAD;
    
    return true;
}



// APPENDIX A: EXTERNAL CODE
void* scta_macro_subset_(size_t size, int at, int length, void *p){
    void *n = scta_macro_new_(size);
    if (!n) return NULL;
    
    if (at < 0) at = SCTA__EXTERN_LEN(p)[0] + at;
    
    if(!scta_insert_array(n, 0, (char *) p + at*SCTA__EXTERN_SIZE(p)[0], length)) return NULL;
    else return n;
}
