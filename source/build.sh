#!/bin/bash

depfailmsg() {
    echo "Dependency not meet: $1"
    echo "$2"
    exit 1
}

cat > magictest.c <<EOF
#include <magic.h>

int main(){
    magic_t mc = magic_open(MAGIC_SYMLINK|MAGIC_MIME_TYPE|MAGIC_NO_CHECK_ENCODING);
    return 0;
}
EOF

hash gcc make || depfailmsg "gcc and/or make" '
    On debian systems, issue the following: 
        apt-get install build-essential
    On redhat systems, issue the following:
        yum groupinstall "Development Tools"
'


gcc -o TT magictest.c -lmagic || depfailmsg ""  "
Ether libmagic's development files are not installed on your system or gcc 
doesn't know how to access them. If you're using:"'
    
    On debian systems, issue the following: 
        apt-get install libmagic-dev
    On redhat systems, issue the following:
        yum install "file-*"
    
'


[[ -x TT ]] && rm TT || exit 1
rm magictest.c

make -s || {
    echo "It appears this program is going to give you a hard time :(
Please contact via the projects bug tracker for help."
    exit 1
}
