/*
    sctstr: A simple string module 

    Sulaiman (seininn) Mustafa     2014-01-19
    Version 0.1
    
    This module is a part of libstc.
*/

#ifndef SCTA_H
#define SCTA_H

#include <stdio.h>
#include <stdbool.h>
#include <limits.h>



/*
‎         .                 لا تستخدم الماكروات التالية واقتصر على الماكروات
‎        ╱ ╲           ‫المعرفة في "PUBLIC API". استغرار هذه الماكروات غير مضمون.
‎       ╱ | ╲
‎      ╱  ·  ╲           Do NOT use the following macros and limit your use
‎     ∙───────∙         to the macros defined under "PUBLIC API". This macros
                                  are not garanteed to be stable.

*/

#define SCTA__OFFSET_SIZE 0
#define SCTA__OFFSET_LEN  (SCTA__OFFSET_SIZE + sizeof(size_t))
#define SCTA__OFFSET_RET  (SCTA__OFFSET_LEN + sizeof(int))
#define SCTA__OFFSET_INT1 (SCTA__OFFSET_RET + sizeof(bool))
#define SCTA__OFFSET_INT2 (SCTA__OFFSET_INT1 + sizeof(int))
#define SCTA__OFFSET_PTR1 (SCTA__OFFSET_INT2 + sizeof(int))
#define SCTA__OFFSET_PTR2 (SCTA__OFFSET_PTR1 + sizeof(void *))
#define SCTA__OFFSET_HEADER  (SCTA__OFFSET_PTR2 + sizeof(void *))

#define SCTA_HEAD SCTA__OFFSET_HEADER



#define SCTA___SIZE(PTR, OFFSET)  ((size_t *) (((char *) PTR) + SCTA__OFFSET_SIZE   + OFFSET))
#define SCTA___LEN(PTR, OFFSET)   ((int *)    (((char *) PTR) + SCTA__OFFSET_LEN    + OFFSET))
#define SCTA___RET(PTR, OFFSET)   ((bool *)   (((char *) PTR) + SCTA__OFFSET_RET    + OFFSET))
#define SCTA___INT1(PTR, OFFSET)  ((int *)    (((char *) PTR) + SCTA__OFFSET_INT1   + OFFSET))
#define SCTA___INT2(PTR, OFFSET)  ((int *)    (((char *) PTR) + SCTA__OFFSET_INT2   + OFFSET))
#define SCTA___PTR1(PTR, OFFSET)  ((void **)  (((char *) PTR) + SCTA__OFFSET_PTR1   + OFFSET))
#define SCTA___PTR2(PTR, OFFSET)  ((void **)  (((char *) PTR) + SCTA__OFFSET_PTR2   + OFFSET))


// returns the size, length, and status of the true bigening of the array (internal)
#define SCTA__INTERN_SIZE(X) SCTA___SIZE(X, 0)
#define SCTA__INTERN_LEN(X)  SCTA___LEN(X, 0)
#define SCTA__INTERN_RET(X)  SCTA___RET(X, 0)
#define SCTA__INTERN_INT1(X) SCTA___INT1(X, 0)
#define SCTA__INTERN_INT2(X) SCTA___INT2(X, 0)
#define SCTA__INTERN_PTR1(X) SCTA___PTR1(X, 0)
#define SCTA__INTERN_PTR2(X) SCTA___PTR2(X, 0)


// returns the size, length, and status of apparent bigening of the array (external)
#define SCTA__EXTERN_SIZE(X) SCTA___SIZE(X, -SCTA_HEAD)
#define SCTA__EXTERN_LEN(X)  SCTA___LEN(X,  -SCTA_HEAD)
#define SCTA__EXTERN_RET(X)  SCTA___RET(X,  -SCTA_HEAD)
#define SCTA__EXTERN_INT1(X) SCTA___INT1(X, -SCTA_HEAD)
#define SCTA__EXTERN_INT2(X) SCTA___INT2(X, -SCTA_HEAD)
#define SCTA__EXTERN_PTR1(X) SCTA___PTR1(X, -SCTA_HEAD)
#define SCTA__EXTERN_PTR2(X) SCTA___PTR2(X, -SCTA_HEAD)

extern FILE *scta_config_debug;
extern int scta_config_version[3];

void* scta_macro_new_(size_t size);
void scta_macro_free_(void *c);
bool scta_macro_set_length_(char **z, int newlength);
bool scta_macro_insert_(char **z);
bool scta_macro_insert_array_(char **z);
bool scta_macro_delete_subset_(char **z);
void* scta_macro_subset_(size_t size, int at, int length, void *p);



/*
            ╭──────────╮
            │          │
            │   ╭──╮   │
            │   ╰──╯   │
            │          │                         ╭────╮ ╭────╮ ╭─╮  
            │   ╭──────╯ ╭─╮    ╭─╮  ╭─╮ ╭──╮    │ ╭╮ │ │ ╭╮ │ │ │
            │   │ ╭─╮╭─╮ │ ╰──╮ │ │  │ │ │ ╭╯    │ ╰╯ │ │ ╰╯ │ │ │
            │   │ │ ││ │ │ ╭╮ │ │ │  │ │ │ │     │ ╭╮ │ │ ╭──╯ │ │
            │   │ │ ╰╯ │ │ ╰╯ │ │ ╰╮ │ │ │ ╰╮    │ ││ │ │ │    │ │
            ╰───╯ ╰────╯ ╰────╯ ╰──╯ ╰─╯ ╰──╯    ╰─╯╰─╯ ╰─╯    ╰─╯
         ⇩ ⇩ ⇩ ⇩ ⇩ ⇩ ⇩ ⇩ ⇩ ⇩ ⇩ ⇩ ⇩ ⇩ ⇩ ⇩ ⇩ ⇩ ⇩ ⇩ ⇩ ⇩ ⇩ ⇩ ⇩ ⇩ ⇩ ⇩ ⇩ ⇩ ⇩ 
*/






#define scta_new(type)                              (scta_macro_new_(sizeof(type)))/*
sctman:scta_new

NAME
    scta_new

SYNOPSIS
    bool scta_new(type)
    
DESCRIPTION
    Takes any type as its input, and returns a pointer to a zero length array
    with elements of the same type.

NOTES
    This function is implemented as a macro.

RETURN
    a pointer of type `type *' on success, and null otherwise.

EXAMPLE
    int *list = scta_new(int);
    char *list = scta_new(char);
    struct foo *list = scta_new(struct foo);
    struct foo **list = scta_new(struct foo *);

*/


#define scta_free                                scta_macro_free_/*
sctman:scta_free
NAME
    scta_free
    
SYNOPSIS
    void scta_free(array)

DESCRIPTION    
    Takes a dynamic array and frees it.

RETURN
    None
*/


#define scta_length(A)                              (SCTA__EXTERN_LEN(A)[0])/*

sctman:scta_length
NAME
    scta_length
    
SYNOPSIS
    int scta_length(array)

DESCRIPTION    
    Returns the length of the array 

RETURN
    The length of `array' as an int.
*/

#define scta_set_length(A, newl)                    (scta_macro_set_length_((char **) &(A), newl))/*

DESCRIPTION    
    expands or collapses an array. If newl is larger than the length of the 
    array, the array is expanded and all new elements are set to zero. If 
    newl is smaller than the length of the provided array, the array is 
    collapsed.
    
RETURN
    A boolian value indicating success or failure; stdbool definitions are used.
    

*/

#define scta_insert(A, where, what)                 ((SCTA__EXTERN_INT1(A)[0]=where),\
                                                     ((A)[*SCTA__EXTERN_LEN(A)]=what),\
                                                     scta_macro_insert_((char **) &(A)))/*
                                                     
DESCRIPTION    
    This macro inserts variable `what' in a dynamic array `A' at index `where'.

RETURN
    A boolian value indicating success or failure; stdbool definitions are used.

EXAMPLE
    int *a; // [1, 2, 3]
    if (!scta_insert(a, 1, 300)) {
        puts("this macro failed");
    }
    a; // [1, 300, 2, 3]
*/
                     
                                                     
#define scta_insert_array(A, where, target, length) ((SCTA__EXTERN_INT1(A)[0]=(where)),\
                                                     (SCTA__EXTERN_INT2(A)[0]=(length)),\
                                                     (SCTA__EXTERN_PTR1(A)[0]=(target)),\
                                                     scta_macro_insert_array_((char **) &(A)))
                                                     
                                                                                             
#define scta_push(A, what)                          scta_insert((A), INT_MAX, (what))

#define scta_push_array(A, target, length)          (scta_insert_array((A), INT_MAX, (target), (length)))

#define scta_delete_subset(A, where, length)        ((SCTA__EXTERN_INT1(A)[0]=(where)),\
                                                     (SCTA__EXTERN_INT2(A)[0]=(length)),\
                                                     scta_macro_delete_subset_((char **) &(A)))

#define scta_pop(A, var)                            (scta_delete_subset((A), INT_MAX, 1) ?\
                                                     ((var)=((A)[scta_length(A)]),true) :\
                                                     false)

#define scta_subset(array, at, length)              (scta_macro_subset_(sizeof((array[0])), (at), (length), (array)))

#endif
