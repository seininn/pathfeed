/*

    sctos: Usefull Operating System Routines
    Sulaiman (seininn) Mustafa
    Version 1:0:1
    Date: 2014-01-21
    Dependency list: sctstr
    
    This module is a part of libstc.
*/

#ifndef SCTOS_H
#define SCTOS_H

#include <stdbool.h>
#include <stdio.h>

extern bool sctos_config_debug;
bool sctos_config_verbose;

extern int sctos_config_version[3];

bool sctos_walk(char *dirname, bool (*handler)(void *payload, char *path), void *payload);
bool sctos_load(char *path, char **buf);

#endif
