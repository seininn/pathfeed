/*
    PAthfeed: A simple tool for representing file system updates
    
    Author: Sulaiman (seininn) Mustafa
    Date:   2014-01-28
    COPY:   See attached statement

    Contact & Bug reports:
        Web - http://seininn.bitbucket.org/contact or bitbucket.org/seininn
        Email - @hush.com, seininn
        Issue tracker - http://bitbucket.org/seininn/pathfeed/issues/new


*/

#define _POSIX_C_SOURCE 200809L

#define version "1.0.1"

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <time.h>
#include <string.h>
#include <libgen.h> // dirname

#include <magic.h>

#include "sctos.h"
#include "scttime.h"
#include "sctml.h"
#include "sctstr.h"
#include "sctframe.h"


magic_t mc; // magic cookie

struct info_s{
    char *feed_title;
    char *feed_url;
    char *feed_author;
    char *feed_email;
    
    char *entry_title_prefix;
    char *entry_url_prefix;

    FILE *outstream;
    bool dynamic;    
    bool verbose;  
    char *mime_whitelist;
    int   mime_count;
    char *source_dir;
    
} info;


bool entry_factory(void *unused, char *path){
    const char *mime;
    struct stat st;
    
    if (unused) return false;
    
    // get mtime and ignore direcories
    if (stat(path, &st) == -1) return false;
    if (S_ISDIR(st.st_mode)) return true;
    
    // magic init
    mime = magic_file(mc, path);
    if (!mime) {
        fprintf(stderr, 
            "pathfeed: Encountered an error when attempting "
            "to open '%s': %s\n", path, strerror(magic_errno(mc)));
        return false;
    }
    
    // check if mime set is provided, if so, check file matches
    if (info.mime_whitelist) {
        bool inlist = false;
        char *mimelp = info.mime_whitelist;
        for (int i = 0;; ++i) {
            if (!strcmp(mimelp, mime)) {
                inlist = true;
                break;
            }
            mimelp += (strlen(mimelp)); 
            
            if (i == info.mime_count) break;
            else mimelp += 1;
        }
        if (!inlist) return true;
    }

    
    // then build xml tree and print to info.output
    char *title_raw = NULL;
    char *title_escaped = NULL;
    char *title_prefix_raw = NULL;
    char *title_prefix_escaped = NULL;
    sctframe_t *f = sctframe_new();
    if (!f) {
        fprintf(stderr, "pathfeed: frame allocation error");
        return false;
    }
    
    
    
     
    // add libexractor when it's API stablizes
    // Extracting title if html
    char *title = NULL;
    if (info.dynamic && !strcmp(mime, "text/html")) {
        char **tags;
        tags = C(f, sctml_get_tags_from_file(path, "title"), sctml_free_tags);
        if (tags[0]) title_escaped = C(f, sctml_strip_tags(tags[0]));
    }
    
    // only apply title prefix if no title was found
    if (info.entry_title_prefix && !title_escaped) title_prefix_escaped = sctml_xml_escape(info.entry_title_prefix, NULL);

    // putting url parts together
    char *abs_url_raw = NULL;
    char *abs_url_encoded = NULL;
    char *abs_url_escaped = NULL;
    char *rel_url_raw = NULL;
    char *rel_url_encoded = NULL;
    char *rel_url_escaped = NULL;
    
    rel_url_raw = path+strlen(info.source_dir);
    
    sctstr_append (&abs_url_raw, info.entry_url_prefix);
    sctstr_append (&abs_url_raw, rel_url_raw);
    
    abs_url_encoded = sctml_percent_encode(abs_url_raw, "-./");
    abs_url_escaped = sctml_xml_escape(abs_url_encoded, NULL);
    
    fprintf( info.outstream, "<entry>\n");
    fprintf( info.outstream, "\t<title>%s%s</title>\n", 
                                        (title_prefix_escaped?title_prefix_escaped:""),
                                        (char *) C(f, 
                                            sctml_xml_escape((title_escaped?title_escaped:path+strlen(info.source_dir)), NULL)
                                        ));
    fprintf( info.outstream, "\t<link href='%s' type='%s'/>\n", 
                                        abs_url_escaped, 
                                        (char *) C(f, sctml_xml_escape((char *)mime, ""))
                                        );
    fprintf( info.outstream, "\t<id>%s</id>\n", abs_url_escaped);
    fprintf( info.outstream, "\t<updated>%s</updated>\n", scttime_rfc3339(st.st_mtime, (char [22]){0}));
    fprintf( info.outstream, "</entry>\n");
    
    
    free(abs_url_raw);
    free(abs_url_encoded);
    free(abs_url_escaped);
    free(rel_url_encoded);
    free(rel_url_escaped);
    sctframe_free(f);
    
    return true;
}

#include <assert.h>
int main(int argc, char **argv){
    /* gcc lolypop */ if (argc == -1) return 1;
    
    
    char *help_message = ""
"       USAGE                                                                   \n"
"           dirrss   [OPTIONS]                                                  \n"
"                                                                               \n"
"       Captial option letters are general feed options:                        \n"
"           -T: feed title                                  Required            \n"
"           -U: feed URL                                    Required            \n"
"           -A: feed author                                 Required            \n"
"           -E: feed email                                  Default: none       \n"
"           -D: Directory to generate the feed from         Default: stdin      \n"
"                                                                               \n"
"           -M: MIME-type whitelist                         Default: all        \n"
"           -F: File to write to                            Default: stdout     \n"
"                                                                               \n"
"       Small option letters are entry options:                                 \n"
"           -t: entry title prefix                          Default: none       \n"
"           -d: attempt to dynamicly determine file title   Default: no         \n"
"           -u: entry url prefix                            Required            \n"
"                                                                               \n"
"       -V prints version and exit                                              \n"
"       -v for verbose mode                                                     \n"
"       -h for help                                                             \n";
            

    int c;
    char *params = "T:U:A:E:M:F:D:t:u:hdvV";
    
    if (geteuid() == 0) {
        fprintf(stderr, "This program should not be run as root. Aborting.\n");
        return 1;
    }
    
    while ((c=getopt(argc, argv, params))!=-1) {
        switch (c) {
            case 'T':
                info.feed_title = optarg;
                break;
            case 'U':
                info.feed_url = optarg;
                break;
            case 'A':
                 info.feed_author = optarg;
                break;
            case 'E':
                 info.feed_email = optarg;
                break;
            case 'M':
                 info.mime_whitelist = optarg;
                 {
                    info.mime_count = 0;
                    for (int i = 0; info.mime_whitelist[i]; ++i) if (info.mime_whitelist[i] == ':') {
                        info.mime_whitelist[i] = 0; 
                        ++info.mime_count;
                    }
                 }
                break;
            case 'F':
                if (info.verbose) fprintf(stderr, "writting output to '%s'\n", optarg);
                info.outstream = fopen(optarg, "w");
                if (!info.outstream) {
                    fprintf(stderr, "Could not open '%s': ", optarg);
                    perror("");
                    return 1;
                }
                break;
            case 'D':
                info.source_dir = optarg;
                break;
            case 't':
                info.entry_title_prefix = optarg;
                break;
            case 'd':
                info.dynamic = true;
                break;
            case 'u':
                info.entry_url_prefix = optarg;
                break;
            case 'v':
                info.verbose = true;
                if (info.verbose) fprintf(stderr, "verbose mode set\n");
                break;
            case 'V':
                puts(version);
                return 0;
            case 'h':
                puts(help_message);
                return 0;
            case '?':
                if (!strchr(params, c)) {
                    fprintf (stderr, "Missing argument for -%c\n", optopt);
                }
                else {
                    fprintf (stderr, "Unknown option -%c\n", optopt);
                }
                return 1;
                break;
            default:
                fprintf (stderr, "should not occur? ( -%c )\n",optopt);
        }
    }
    
    
    
    /* Initialization and validation
        Reffer to required arguemnts above.
    */
    // Checking that manditory arguments are supplied.
    if (info.verbose) fprintf(stderr, "checking for manditory options\n");
    char *fancy_manditory_check[8][2];
    fancy_manditory_check[0][0] = info.feed_title;          fancy_manditory_check[0][1] = "-T";
    fancy_manditory_check[1][0] = info.feed_url;            fancy_manditory_check[1][1] = "-U";
    fancy_manditory_check[2][0] = info.feed_author;         fancy_manditory_check[2][1] = "-A";
    fancy_manditory_check[3][0] = info.entry_url_prefix;    fancy_manditory_check[3][1] = "-u";
    for (int i = 0; i < 4; ++i) if (!fancy_manditory_check[i][0]) {
        fprintf(stderr, "%s is required. See -h for further details.\n", fancy_manditory_check[i][1]);
        return 1;
    }
    
    // Checking optionals and providing defaults
    if (!info.outstream) info.outstream = stdout;
    
    // initializing libraries and other subsystems
    if (info.verbose) fprintf(stderr, "initializing libmagic\n");
    if (!(mc = magic_open(MAGIC_SYMLINK|MAGIC_MIME_TYPE|MAGIC_NO_CHECK_ENCODING))) {
        fputs("libmagic failed to initialize\n", stderr);
        return 1;
    }
    magic_load(mc, NULL);

        
    /* Logic Starts here
    */
    
    //FIXME write to outfile not stdout
    // here print title and stuff see dirrss or atom spec

    info.feed_url = sctml_xml_escape(info.feed_url, NULL);
    info.feed_title = sctml_xml_escape(info.feed_title, NULL);
    info.feed_author = sctml_xml_escape(info.feed_author, NULL);
    if (info.feed_email) info.feed_email = sctml_xml_escape(info.feed_email, NULL);
    
    if (info.verbose) fprintf(stderr, "printing feed header\n");
    fprintf(info.outstream, "<?xml version=\"1.0\" encoding=\"utf-8\"?>"
        "<feed xmlns=\"http://www.w3.org/2005/Atom\">"
        "<id>%s</id>" 
        "<link href=\"%s\" rel=\"self\"/>" 
        "<title>%s</title>" 
        "<updated>%s</updated>" ,
        info.feed_url,
        info.feed_url,
        info.feed_title,
        scttime_rfc3339(time(NULL), (char [22]){0}));
        
    fprintf(info.outstream, "<author><name>%s</name>",info.feed_author);
    if (info.feed_email) fprintf(info.outstream, "<email>%s</email>", info.feed_email);
    fprintf(info.outstream, "</author>");
        
    free(info.feed_url);
    free(info.feed_title);
    free(info.feed_author);
    free(info.feed_email);
    
    bool status = true;
    if (!info.source_dir) { // Read path list from stdin
        char *input = NULL;
        if (info.verbose) fprintf(stderr, "entered stdin mode...\n");
        while (getline(&input, (size_t [1]) {0}, stdin) != -1) {
            // remove a newline if present
            for (int i = 0; input[i]; ++i) if (input[i]=='\n') {
                input[i] = 0;
                break;
            }
            // Consider the first line as the source directory
            if (!info.source_dir) { 
                if (!(info.source_dir = malloc(strlen(input)+2))) {
                    fprintf(stderr, "out of memory\n");
                    exit(1);
                }
                strcpy(info.source_dir, input);
                if (info.source_dir[strlen(info.source_dir)-1] == '/') {
                    info.source_dir[strlen(info.source_dir)-1] = 0;
                }
            }
            status &= entry_factory(NULL, input);
            free(input);
        }
        free(input);
        free(info.source_dir);
    }
    else {
        if (info.verbose) fprintf(stderr, "traversing %s\n", info.source_dir);
        status = sctos_walk(info.source_dir, entry_factory, NULL); 
    }
    
    fputs("</feed>", info.outstream);
    if (!status) {
        fprintf(stderr, "A problem was encountered while processing paths "
                        "from '%s'. Feed may not be complete.\n", 
                        (info.source_dir?info.source_dir:"stdin"));
    }


    // Done
    magic_close(mc);
    if (info.outstream != stdout) fclose(info.outstream);
    
    if (info.verbose) fprintf(stderr, "writting buffered streams\n");
    fflush(info.outstream);
    if (info.verbose) fprintf(stderr, "done\n");
    return 0;
}
