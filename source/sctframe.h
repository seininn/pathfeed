#ifndef SCTFRAME_H
#define SCTFRAME_H

extern FILE *sctframe_config_debug;
extern int sctframe_config_version[3];

struct record {
    void *address;
    void (*freef)(void *);
};

typedef struct {
    struct record *records;
} sctframe_t;


sctframe_t *sctframe_new();
void *sctframe_add_(sctframe_t *f, ...);
void sctframe_free(sctframe_t *f);

#define C(f, ...) (sctframe_add_(f, __VA_ARGS__, NULL))

#endif
