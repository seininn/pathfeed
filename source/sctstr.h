/*
    sctstr: A simple string module 

    Sulaiman (seininn) Mustafa     2014-01-19
    Version 0.1
    
    This module is a part of libstc.
*/
#ifndef SCTSTR_H
#define SCTSTR_H

#include <stdbool.h>
extern FILE* sctstr_config_debug;

bool sctstr_append(char **o, char *s);

#endif

