default:
	cd source; make
	cp source/pathfeed .

private-update:
	# update package dir
	@cd package; cript seininn ../source pathfeed
	@cp README COPY NEWS package
	
	# update web with new files, packages, etc
	@bash -c 'cd package; tar -zcf "../web/downloads/pathfeed.tar.gz" *;cd ../web/downloads; cp pathfeed.tar.gz pathfeed-`../../package/pathfeed -V`.tar.gz' # versions!
	
	# send to public directory and update feeds
	@cp -r web/* /me/public/projects/pathfeed # copy project website to public
	@cd /me/public; bash update-website.sh #issues a website rebuild
	
	@echo "done"
