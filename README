
                                   Pathfeed
              Represent directories and files as atom feed entries
                                version 1.0.0
                             Sulaiman A. Mustafa

                                 


  ______________________/INTELLECTUAL PROPERTY NOTICE\________________________
 |                                                                            |
 | License/Copyright: See the file COPY in the base directory of this project |
 |____________________________________________________________________________|



DESCRIPTION
-----------
Pathfeed is a command line tool that represents directory content as an atom
feed. It can also do the same with paths supplied through standard input.


Features:
    A.  Supports MIME type white-listing. Only include files that match the
        list of supplied MIME types.
    B.  Supports title extraction for some formats: Currently this is only
        done for html files, but this might change soon.
    C.  cript package included (shameless plug)

REQUIREMENTS
------------
Currently, pathfeed depends on lib magic for file MIME detection. Running 
pathfeed requires that a libmagic shared object be present on your system 
or that libmagic is statically linked.

The dependency list might get longer (or shorter) with future releases.


COMPILING
---------
The projects source distribution provides to ways to use pathfeed: The first
method requires that you build the software by issuing make. The other is to 
use the cript wrapper (a self-building source-code package) which does 
everything for you.

Either method requires that the development files for libmagic be installed on
your system. On debian systems, these files are provided by `libmagic-dev'.

To manually build pathfeed, change to the directory `source' and issue `make'.
The cript wrapper can be used immediately as if it was the compiled binary.


USAGE
-----
The atom specification requires the presence of a few elements for the feed to
be valid. These elements are implemented as mandatory options that must be 
supplied when pathfeed is invoked. See -h for further information. 

The following examples yield the same result. All command arguments should be 
on a single line.

    find -L /home/$USER/Downloads | ./pathfeed 
        -T "My downloads"                   # Feed Title (Required)
        -A "$USER"                          # Feed Author (Required)
        -U "http://example.com/feed.xml"    # Feed location (Required)
        -u "http://example.com/files"       # URL prefix for file paths (Required)
        -M "text/html:video/mp4"            # MIME whitelist
        -d                                  # Determine titles dynamically
        > "feed.xml"                       
        
    ./pathfeed 
        -T "My downloads"                   
        -A "$USER"                          
        -U "http://example.com/feed.xml"    
        -u "http://example.com/files"       
        -M "text/html:video/mp4"            
        -D "/home/$USER/Downloads"          # Directory to traverse
        -F "feed.xml"                       # Output file
        -d                                  


Note: if you're piping paths to pathfeed, pathfeed will consider the first path
      as the base directory (equivalent to passing it as an argument with -D).

Support and Help
----------------
While I can't promise prompt responses and 24/7 hot lines, I'll try my best to 
reply in a timely fashion. If you don't receive a reply, it might be because I 
switched to another provider again. Try other communication channels such as the
issue tracker (please include your email with the bug report so that I can get 
back to you if I need more information).


Contact Information
-------------------
Personal: http://seininn.bitbucket.org/contact
Issue tracker: http://bitbucket.org/seininn/pathfeed/issues/new

