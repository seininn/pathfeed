#!/bin/bash

[[ "$1" == "clean" ]] && {
    rm *.xml *.xml.flat 2>&1 > /dev/null
    echo "clean" 
    exit
}

hash rxp xml2 || {
    echo "This script uses rxp and xml2 to handle library output testing. Abort."
    exit 1
}

[[ -x ../source/pathfeed ]] || {
    echo "Could not find the pathfeed executable in '../source'. build pathfeed then rerun this script in the test directory."
    exit 1
}

run_pathfeed_with_args(){
    args=$(cat "$1")
    shift
    ../source/pathfeed ${args[@]} $@ || {
        echo "pathfeed exited with a non-zero exit code."
        exit 1
    }
}

validate_xml(){
    rxp -Nxs "$1" || {
        echo "'$1' is not valid xml!"
        exit 1
    }
}

gen_flat_xml(){
    cat "$1" | xml2 | grep -v updated | sort > "$1".flat || {
        echo "failed to flatten '$1'"
        exit 1
    }
}


compare_with_result(){
    diff "$1" "$1.result" || {
        echo "Failed test $1. abort."
        exit 1
    }
}

echo -n "running general tests"
# General test
run_pathfeed_with_args 2.args -F 2.xml -D example.dir
validate_xml 2.xml 
gen_flat_xml 2.xml 
compare_with_result 2.xml.flat
echo -n "..."

# Same as 1 but with streams
find -L example.dir | run_pathfeed_with_args 2.args > 2.xml 
validate_xml 2.xml 
gen_flat_xml 2.xml 
compare_with_result 2.xml.flat
echo -n "..."

# without dynamic titiling
run_pathfeed_with_args 3.args
validate_xml 3.xml 
gen_flat_xml 3.xml 
compare_with_result 3.xml.flat
echo -n "..."

# basic
run_pathfeed_with_args 4.args -F 4.xml -D example.dir
validate_xml 4.xml 
gen_flat_xml 4.xml 
compare_with_result 4.xml.flat
echo -n "..."

# Same as before but with streams
find -L example.dir | run_pathfeed_with_args 4.args > 4.xml 
validate_xml 4.xml 
gen_flat_xml 4.xml 
compare_with_result 4.xml.flat
echo "done"

#only validation

echo -n "generating big directory..."
mkdir example.2.dir
for i in {1..20000}; do echo "$i" > "example.2.dir/$i"; ! ((i%1000)) && echo -n "."; done
echo "done"


echo -n "generating big atom file..."
run_pathfeed_with_args 5.args -F 5.xml
echo "done"

echo -n "running big validation..."
validate_xml 5.xml
xml2 < 5.xml | grep 19999 > /dev/null || {
    echo "missing last entry in 5.xml"
    exit 1
}
echo "done"

rm -r *.xml *.xml.flat example.2.dir
echo "ok"
